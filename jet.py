from os import path
from typing import Dict, Any

import pygame
from enum import Enum

import randomdict as randomdict


class Direction(Enum):
    UP = 1
    DOWN = 2
    RIGHT = 3
    LEFT = 4


# директория с картинками
img_dir = path.join(path.dirname(__file__), 'sources')

# словарь с космическими кораблями
jets = randomdict.RandomDict(
    jet_1=pygame.image.load(path.join(img_dir, "jet_1.png")).convert_alpha(),
    jet_2=pygame.image.load(path.join(img_dir, "jet_2.png")).convert_alpha(),
    jet_3=pygame.image.load(path.join(img_dir, "jet_3.png")).convert_alpha())

# словарь с фонами
backgrounds = randomdict.RandomDict(
    background_1=pygame.image.load(path.join(img_dir, "background_1.png")).convert_alpha(),
    background_2=pygame.image.load(path.join(img_dir, "background_2.png")).convert_alpha(),
    background_3=pygame.image.load(path.join(img_dir, "background_3.png")).convert_alpha())


# блоки, из которых состоит змейка
class Jet(pygame.sprite.Sprite):
    def __init__(self, image, direction, x, y, velocity, acceleration):
        pygame.sprite.Sprite.__init__(self)
        self.direction = Direction.RIGHT
        self.image = jets[image]
        self.image.size = self.image.size * 0.5
        self.rect = self.image.get_rect()

        self.rotate(direction)

        self.rect.x = x
        self.rect.y = y

        self.velocity = 5
        self.acceleration = 0
        #self.image = pygame.transform.scale(self.image, (config.BLOCKSIZE, config.BLOCKSIZE))
        #self.image = pygame.transform.rotate(self.image, 270)


    def rotate(self, direction):
        if (direction == Direction.RIGHT):
            self.image = pygame.transform.rotate(self.image, -90 + self.direction)
            return
        elif (direction == Direction.LEFT):
            self.image = pygame.transform.rotate(self.image, -270 + self.direction)
            return
        elif (direction == Direction.UP):
            self.image = pygame.transform.rotate(self.image, self.direction)
            return
        elif (direction == Direction.DOWN):
            self.image = pygame.transform.rotate(self.image, -180 + self.direction)
            return

    def position(self):
        self.x += self.velocity * time
        self.x += self.velocity * time
        
    def update(self):
        self.rect.x += self.dx
        self.rect.y += self.dy

    def draw(self, screen):
        self.sprites.draw(screen)